# Join deliberate - Backend Challenge

Thanks for your interest in joining Deliberate! Please, check our open positions at https://www.deliberate.ai/careers

## Goals

This coding challenge was designed to allow you to show us how good you are with Java or Kotlin! The purpose of this exercise is to demonstrate your ability to build a greenfield project with what you consider best practices.

## Overview

Data collection is critical to objective measures for mental health diagnosis and treatment. As a company, we are determined to do everything we can to provide a data-driven perspective to those who need it most.

## Assignment

We take privacy seriously and having patient consent is crucial for us. Your job will be to create a web application that allows clinics to upload patients' consent documents, verify that the documents have been signed, and mark documents as verified.

### Feature

Provide the following:

- Create the consent document CRUD APIs
- Create an API that allows a clinic to mark documents as verified or invalid
- Automate a process to flag as pending all consent forms that are not verified in 3 business days

Note: We will **NOT** evaluate frontend skills

### Data

At least, the following fields are required:

- Clinic data
- Patient email
- Uploaded at
- Updated at
- Verified at

## Guidelines

The challenge is intentionally kept very open. Feel free to make your own decisions and be creative. If you don't get to work on everything, outline what the next steps would look like and how you would approach them.

**Important**: You can use any Java/Kotlin libraries you wish. The only requirement is that you use HTTP for communication.

Questions to consider:

- Does it work as expected?
- Are there automated tests?
- Does the code follow well know best practices?
- Is it well documented? What is the building and deploying process?
- Is the application using resources properly? What happens if the user types too fast?
- Are there any security vulnerabilities?
- Is it production-ready?
- Did I document my decisions?
- If something is missing, does the README explain why it is missing?

# Submission

Once you are comfortable with your code, please provide the GitLab or GitHub link. If it is a private one, please invite dev@deliberate.ai.

This is a limited-time challenge. Please, submit it within the time window discussed via email.

# Extra Challenge

- Create an API to upload the document using a public cloud provider such as AWS, Azure or Google
